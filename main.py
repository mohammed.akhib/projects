class My_Calculator:
    def __init__(self):
        pass
    def SUM(self, x, y):
        return x + y
    def DIFFERENCE(self, x, y):
        return x - y
    def MULTIPLICATION(self, x, y):
        return x * y
    def DIVISION(self, x, y):
        if y == 0:
            raise ValueError("Cannot divide by zero")
        return x / y
if __name__ == "__main__":
    calculator = My_Calculator()
    while True:
        print("Select operation:")
        print("1. SUM")
        print("2. DIFFERENCE")
        print("3. MULTIPLICATION")
        print("4. DIVISION")
        print("5. Exit")
        choice = int(input("Enter choice (1-5): "))
        if choice == 5:
            break
        num1 = int(input("Enter 1st number: "))
        num2 = int(input("Enter 2nd number: "))
        if choice == 1:
            result = calculator.SUM(num1, num2)
            print("Result: ", result)
        elif choice == 2:
            result = calculator.DIFFERENCE(num1, num2)
            print("Result: ", result)
        elif choice == 3:
            result = calculator.MULTIPLICATION(num1, num2)
            print("Result: ", result)
        elif choice == 4:
            result = calculator.DIVISION(num1, num2)
            print("Result: ", result)


